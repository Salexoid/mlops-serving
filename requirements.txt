ultralytics
python-dotenv>=0.5.1
mlflow==2.12.2
boto3==1.34.106
psycopg2-binary==2.9.9
gradio
opencv-python