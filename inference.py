import os
import shutil
import gradio as gr
import logging
import mlflow
from ultralytics import YOLO
from dotenv import find_dotenv, load_dotenv


class YoloWrapper:
    def __init__(self, model_name='yolov8_yolov8_training_dvc_test_20_640.pt', model_stage='Production'):
        """
        To initialize the model
        model_name: Name of the model in registry
        model_stage: Stage of the model
        """

        registry_address = os.getenv('MLFLOW_TRACKING_URI')
        logging.info(f'Loading the model from Registry: {registry_address}')
        mlflow.set_tracking_uri(registry_address)
        client = mlflow.MlflowClient(mlflow.get_tracking_uri())
        model_info = client.get_latest_versions(model_name)[0]

        logging.info(f'Got model info: {model_info}')
        mlflow.artifacts.download_artifacts(artifact_uri=model_info.source, dst_path='.')
        self.model = YOLO('best.pt')

def detect_objects(model_name, input_image):
    res_dir = '/usr/src/ultralytics/runs/detect/predict'
    if os.path.exists(res_dir) and os.path.isdir(res_dir):
        shutil.rmtree(res_dir)
    yolo = YoloWrapper(model_name).model
    results = yolo.predict(input_image, save=True)
    logging.info(results)
    res_image_path = f'{res_dir}/{os.path.basename(input_image)}'
    return res_image_path


def launch_gradio():
    load_dotenv(find_dotenv('.env'))
    with gr.Blocks() as demo:
        gr.Markdown("# Object Detection with YOLO")
        with gr.Row():
            with gr.Column():
                model_name = gr.Textbox(label="Model Name", value='yolov8_yolov8_training_dvc_test_20_640.pt')
                input_image = gr.Image(label="Input Image", type="filepath", value='samples/tst.jpg')
                detect_button = gr.Button("Detect")
            with gr.Column():
                output_image = gr.Image(label="Output Image")

        detect_button.click(fn=detect_objects, inputs=[model_name, input_image], outputs=output_image)

    demo.launch(server_name="0.0.0.0", server_port=int(os.getenv('GRADIO_SERVER_PORT', 7860)))

if __name__ == "__main__":
    launch_gradio()
