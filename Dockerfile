FROM ultralytics/ultralytics:latest-arm64

WORKDIR /app

COPY . .
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements.txt
EXPOSE 7860
ENV GRADIO_SERVER_PORT=7860

CMD ["python", "inference.py"]